package com.example.alxcancado.teste;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.stetho.Stetho;

public class MainActivity extends AppCompatActivity {

    TextView textLoaded;
    Button buttonSavePrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        Stetho.initializeWithDefaults(this);



        textLoaded = (TextView) findViewById(R.id.textLoaded);

        buttonSavePrefs = (Button) findViewById(R.id.buttonSavePrefs);
        buttonSavePrefs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // do something
                textLoaded.setText("Aloha!");
                writeOnSharedAndLaunch("com.its4apps.answers");
                //writeOnSharedAndLaunch( getApplicationContext().getPackageName() );





            }
        });
    }


    public void writeOnSharedAndLaunch(String packageName) {

        Log.i("package name: ",packageName);

        PackageManager pm = this.getApplicationContext().getPackageManager();
        boolean isInstalled = isPackageInstalled(packageName, pm);

        if (!isInstalled){
            textLoaded.setText("WARNING! Baixar o Answers");
        } else {
            textLoaded.setText("CALL Answers now!");



            try {
                Context context = createPackageContext(packageName, 0);
                SharedPreferences pref = context.getSharedPreferences(packageName + ".v2.playerprefs", Context.MODE_PRIVATE);

                Log.i("before:", "======= READING ========");
                String action = pref.getString("action", "none");
                String quizId = pref.getString("quiId", "00000");
                String token = pref.getString("its4AuthToken", "empty");

                Log.i("Answers action: ", action);
                Log.i("Answers quizId: ", quizId);
                Log.i("Answers token: ", token);

                pref.edit().putString("action", "config").commit();
                pref.edit().putString("quizId", "884383").commit();
                pref.edit().putString("its4AuthToken", "KIFgy2Pe8vHz_xOucEeIidS50lzIagNiY-eoisXJOPuQOeFH47mnCxhMXBsteuQSWCLBTw29cyYMfT4LC3Mz-dEUw2fp-QMhJSk2WqU9hea46ebQJoHi2LQt6hkJ4d_EgyuR9xt65QDnb78saqN3Defyx6sRLFQ5O2MAmTsioRXEuFBo_fpt_FEt-Ao61EctkZwJGOtxBYmvHflK2QULFLjrYF71D4eXnoq23d4aPEI4Aft47HhcQYE235RaJFGsW2yJcFvWnzLt4R0HlaEL60bQY__yNemM9TrsMbsRk5IW3tA5m2wPsaBwAwsdaa0pZNFKB47FsuwYAoL7h1lzHpub0T4YEwEBJHAaWppQwrUhPaGbPS7KeH1rGt0CRo7QPXMcbhRIwZU6BwA3JJ2_2yodNslbNa-m1YQqJhs-7Um9SP2rUMpHDbvDvSz0kiyj8hfrUdIWn9a4FsfjyNGYRBtLvjLdAnfRXC1dOSaYhcF-i59lXaiYnG27RP7JvLhye2eVbxo44X0KZyIuYq-mmQ").commit();


                //SharedPreferences outro = getSharedPreferences(packageName + ".v2.playerprefs", Context.MODE_PRIVATE);
                //SharedPreferences.Editor outroEdit = outro.edit();
                //outroEdit.putString("action", "event #1");
                //outroEdit.putString("quizId", "13123123123");
                //outroEdit.putString("its4AuthToken", "safbhsfbjsfbsjdfbshjdfbjhsdbfjhsdbfsjdh");
                //outroEdit.commit();

                action = pref.getString("action", "none");
                quizId = pref.getString("quizId", "none");
                token = pref.getString("its4AuthToken", "none");

                Log.i("after:", "======= NEW ========");
                Log.i("Answers action: ", action);
                Log.i("Answers quizId: ", quizId);
                Log.i("Answers token: ", token);

                // call Answers now
                Intent intent = getApplicationContext().getPackageManager().getLaunchIntentForPackage(packageName);
                startActivity(intent);

            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }



            // This should be the SharedPreference file name created by Unity
            /* String sharedPreferenceName = packageName + ".v2.playerprefs";

            Context context = null; // createPackageContext(“unity.bundle.com.example”, 0)
            try {
                context = createPackageContext(packageName, Context.MODE_PRIVATE);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            if (context == null){
                textLoaded.setText("WARNING! Baixar o Answers");
            }


            // This should be the SharedPreference file name created by Unity
            //String sharedPreferenceName = packageName + ".v2.playerprefs";

            // Get the SharedPreferences file
            //SharedPreferences sharedPreferences = context.getSharedPreferences(sharedPreferenceName, MODE_PRIVATE);
            SharedPreferences sharedPreferences = context.getSharedPreferences(this.getApplicationContext().getPackageName(), MODE_PRIVATE);

            // Do some recording work here
            sharedPreferences.edit().putString("action", "play or config");
            sharedPreferences.edit().putString("quizId", "832837 coloca o id do quiz");
            sharedPreferences.edit().putString("its4AuthToken", "coloca o token do usuario para logar no game");
            sharedPreferences.edit().apply();

            // for Debug
            loadSharedPrefs(sharedPreferenceName);
            textLoaded.setText( "from Answers: " + sharedPreferences.getString("quiId","" ) );

            Log.i("package name: ",packageName);

            // Launch Answers App
            //Intent intent = getApplicationContext().getPackageManager().getLaunchIntentForPackage(packageName);
            //startActivity(intent); // */
        }

        /* Context context = null; // createPackageContext(“unity.bundle.com.example”, 0)
        try {
            context = createPackageContext(packageName, Context.MODE_PRIVATE);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if (context == null){
            textLoaded.setText("WARNING! Baixar o Answers");
        }
        */

        // String sharedPreferenceName = context.getPackageName() + ".v2.playerprefs"; //String sharedPreferenceName = context.getPackageName() + ".v2.playerprefs";
        // SharedPreferences sharedPreferences = context.getSharedPreferences(sharedPreferenceName, 0);


        // editor.putString("action", "play or config");
        // editor.putString("quizId", "832837 coloca o id do quiz");
        // editor.putString("its4AuthToken", "coloca o token do usuario para logar no game");
        // editor.apply();

        // loadSharedPrefs(sharedPreferenceName);

        //Intent intent = new Intent(context, UnityPlayerActivity.class);
        //Intent intent = getApplicationContext().getPackageManager().getLaunchIntentForPackage(packageName);
        //startActivity(intent);

    }

    private boolean isPackageInstalled(String packagename, PackageManager packageManager) {
        try {
            packageManager.getPackageInfo(packagename, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public void loadSharedPrefs(String ... prefs) {

        // Logging messages left in to view Shared Preferences. I filter out all logs except for ERROR; hence why I am printing error messages.

        Log.i("Loading Shared Prefs", "-----------------------------------");
        Log.i("----------------", "---------------------------------------");

        for (String pref_name: prefs) {

            SharedPreferences preference = getSharedPreferences(pref_name, MODE_PRIVATE);
            for (String key : preference.getAll().keySet()) {

                Log.i(String.format("Shared Preference : %s - %s", pref_name, key),
                        preference.getString(key, "error!"));

            }

            Log.i("----------------", "---------------------------------------");

        }

        Log.i("Finished Shared Prefs", "----------------------------------");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
